# Changelog

This changelog will keep track of changes within Akane. Changes during October 2019 - August 2021 are not kept, because I didn't think of making a changelog back then, stupid, I know :c

Date Format: MM/DD/YYYY

## WIP
- Toggle usage of commands in a channel (`/admin denychan`, `/admin allowchan`)
- Better Handling for User Retainer Objects (Mostly to save CPU and Memory ~)
- Find Retainer by ID by using `/vr`
- Add/Remove Images from retainers by command (for admins)
- Level Leaderboards
- Retainer Upgrades

## [2.3.7] - 03/15/2022

## Fixed
- `/exchange`: You could duplicate retainers as the select_menu was not retracted after first use
- `/bank info`: Typo which led to an unrelated command being invoked
- Other Minor Fixes

## [2.3.6] - 03/11/2022

## Changed
- `/gr`: show **claimed** event on message edit rather than sending a followUp (to prevent rate limits and spamming)
- `/balance`: command should now show both nekoin and gold balance
- `/market`: fixed spacing errors and made displayed HP for readable

## [2.3.5] - 03/02/2022
### Changed
- `/gr`: increased time to claim from **120000** => **150000** ms
- Market History (`/market`) is reversed to display New to Oldest (Top to Bottom)
- Renamed `/viewretainer` to `/vr` in `/topretainer`
- Update old commands on `/help`
- Using `/exchange` will now put new retainers into your `/list` rather than your main retainer slot
- Changed error messages to ephemerals as a way to reduce ratelimits
- And some code cleanups I guess owo

### Fixed
- Fixed `/arena` not being able to fetch enemy's formations
- Fixed UUIDs not being generated for retainers obtained via `/fuse`
- Fixed User Client not generating errors for numbers out of range (Issue with builders)
- Fixed all user retainers that did not have a UUID auto generated for them
- Fixed Retainer Purchased from `/market` does not transfer over
- Fixed `/dice` generating integers out of range

## [2.3.0] - 02/24/2022

### Added
- Allow using Retainer's Global ID as a search term for `/vr`
- Change the image if the retainers you own (if applicable), with `/rset image`

### Changed
- Renamed Ratings to a more intuitive setup (OLD => NEW | SPECIAL => SS | SS => S)
- Replaced `/usershop` with `/market`, a more superior and intuitive marketplace to buy/sell retainers globally
- Removed old pull leveling system in favor of the new XP leveling system

### Fixed
- Not a single MMORPG commands worked for new players, Fixed that, I've never noticed until today, I'm extremely sorry for this!
- Retainer Format in `/list` was showing incorrectly
- Wrong command was provided when entering the wrong filter for `/list`

## [2.2.0] - 02/02/2022

### Added
- Edit arena formations with `/formation`, it will track the retainer using a UUID, so it'll use the same retainer no matter which slot it is!
- Banking System within `/bank` with weekly interest rates~ 

### Changed
- Calculation System has been changed for `/arena`, it will only calculate from your retainer's MaxHP rather than HP
- Code Optimization within `/arena`, also now shows the time left before the arena tickets refill

## [2.1.0] - 01/23/2022

### Changed
- Merged `/wish`, `/wishremove`, `/wishlist` into subcommands within `/wish`, e.g: `/wish add`, `/wish remove`, `/wish list`
- Code cleanup within `/profile`

### Fixed
- Fixed an exploit within `/exchange` which allowed users to spam `/exchange` to keep re-rolling the retainers on the shop
- Fixed bug where it mistakenly assigns a new UUID to an already owned retainer when using `/clear`
- Fixed an issue where production and development code would coincide with each other
- Fixed issue where errors were not logged due to a typo.

## [2.0.1] - 01/12/2022

### Changed
- Added Unique IDs for claimed retainers for upcoming future updates which require **EXACT** retainer validation from a user
- More code cleanups, not exactly optimization but because I can't stop cringing at old code
- Removed NSFW Commands, this is because people love the commands so much, akane was severely rate limited, which leads the commands to not work 60% of the time

## [2.0.0] - 01/01/2022 - 01/07/2022

### Announcement
- Akane is now hosted on a new VPS in ***Montreal, Canada***, safe to say, it will be more consistent in latency! Sadly Akane only supports (/) Slash Commands~ So use / as the her prefix! Also in this update, I completely removed any frameworks I used previously, and rewrote Akane by scratch so I won't be limited by what a framework can or cannot do, obviously using Discord.JS, because I'm too stupid to learn any hard languages 

### Added
- Added profile cards with retainer statistics in `/profile`, including XP and Leveling (gained from MMORPG commands)
- Added `/stats` to keep track of the bot statistics!
- Added `/sort` which can help sort your retainerlist for you (available in 3 presets)
- Added `/shop`, a shop that sells Akane related items and consumables! (Health Potions and Stat Increase Spells)
- Added `/recycle` because previously you had useless **C** retainers that are just stuck with you forever LOL
- Added `/campaign` as a small, but boring adventure mode for Akane using Retainers!
- Added `/inventory`, you can consume items from your inventory!
- Added `/help`, with options to dive further into a command~

### Changed
- Renames
  - Renamed `/retainerlist` (k.rl previously) to `/list`
  - Renamed `/animecharacters` to `/animechar`
  - Renamed `/smr` (k.smr previously) to `/swap`
  - Renamed `/getretainer` => `/gr`
  - Renamed `/clearmainslot` => `/clear`
  - Renamed `/pay` => `/transfer`
  - Renamed `/codeclaim` => `/claim` for giveaway claims

- Updates 
  - Added a filter for `/list` (previously `/retainerlist`)
  - Created a cooldown handler for Akane
  - Replaced voteReset handler for a more resource efficient function embedded into the command itself, so hopefully no more random stutters~ 
  - Changed how `/fuse` calculates HP for Ascended (Star) retainers by using percentages. It won't use random HPs anymore (Ascended Retainers ONLY)
  - Improved Buttons for `/topretainer`
  - Updated Old UserShop to `/usershop`, this is where you can buy retainers from other users.
  - Updated `/retainerlist` by removing the 50 retainer maximum limit, now you can have as much as you want as long as you purchase the tokens, also improved UI.
  - Optimized `/retainer` with performance improvements, literally reduced the code from 500, down to 160 lines lmao
  - Updated `/gr` with optimizations, code cleaning and removed the ugly colors when claiming a retainer.
  - Updated `/fuse` with a rating filter as a parameter, still limited by 25 retainers
  - Massive performance optimizations for `/arena` using cache and code cleanups
  - I forgor what I changed with `/hug` but I did
  - Updated `/profile` with a more, simplier look?
  - Updated `/top` to combine both **arena** and **nekoin** leaderboards, also optimized it (~4x) with cache, and added paginated it!
  - Updated `/dice` to use two dices rather than just one
  - `/anime`, `/animecharacter`, `/animeseason` now take interaction options as a parameter.

- Code Cleanups
  - Cleaned up **A LOT** of bad code that I've ignored for 2 years now because I was inexperienced asf
  - Massive performance improvements for some MMORPG commands!

### Fixed
- Fixed `/gr` not claiming WHEN you have no main retainer.
- Fixed `/osu` accuracy which for some reason were showing numbers which are far from possible
- Fixed bugs within `/trade` that prevented users from being able to trade retainers with each other
- Fixed `/gr` claim button not claiming due to the pathetic code I made 2 years ago. Also now you can claim within 60 seconds even if your retainerlist is full!
- Fixed extremely annoying bug with `/exchange`

## 19/12/2021 (2 months after Covid)

### Announcement
- Because of Discord's upcoming enforcement on the limitation of contents for messages that bots can see, sooner or later I have to make slash commands, so I'll set a deadline for myself~ February 1, 2022! Hopefully Akane 2.0 will be released without it being a buggy of a mess as Akane is now~ Incomplete maybe~ But please don't be buggy~ 

## 19/10/2021

### Notes
- I am still recovering from corona, but I am trying my best to fix bugs that require fixing.
  
### Changed
- Fixed `k.rl` showing page as undefined for users who’ve never bought new slots before!
- Restyled `k.gr` to be a little bit more pleasing to the eyes >w<

## 13/07/2021

### Added
- Added `k.addr` for **Approved Contributors** to add new retainers to Akane!

### Changed
- Removed `k.confirmtrade`
- Removed `k.removetrade`
- Removed `k.removeshop / k.forceunlist`, there's a dedicated button in shop now.

## 12/07/2021

### Changed
- Optimized claim on `k.gr`, it runs a bit faster when clicking claim, ping is still an issue as Akane is hosted in Asia, the average ping will be around 300ms when using `k.ping`. Sorry about that. **(Outdated as of 01/01/2022, Akane is hosted in North America.)**
- Optimized `k.fuse` button response time, but this is as said above, limited by the ping, sorry...

## 10/07/2021

### Changed
- Changed misleading text on `k.fuse`
- Allow trades between users with no retainers on `k.trade`. Fixed trade bugs. Increased trading time limit from **30 seconds** to **60 seconds**!
- Minor Fix for `k.smr`

## 10/06/2021

### Changed
- On the select menu, ratings will be shown first before the retainer name on `k.fuse`, because on mobile, the retainer name is too long and is hidden/glitched.
- Improved how `k.trade` works, now uses a simple reaction for both users to react to agree to a trade. The trade command also has been optimized to work faster than before.
- Fixed `k.top` pages not working properly!
- Added filters for `k.top`. Examples being: `k.top common` and `k.top rare`
- `k.smr` will now show ratings during changes to better keep track of which retainers you’re handling!

## 10/02/2021

### Added
- Created Changelog, can be accessed with `k.changelogs` (Google Documents)

### Changed
- QOL for `k.fuse`, you no longer need to use codes to fuse anymore, it will do it immediately for you!
- Fixed `k.cmr` (Clear Main Retainer) going above maximum purchased slots
- Cleaner Layout for `k.rl` (Retainer List)
- Fixed Arena Values returning null when user has no main retainer

## 9/24/2021

### Added
- Added `k.cmr` (Clear Main Slot) Command to quickly empty out your main retainer
- Added Top Retainers Command with `k.top`

### Changed
- Updated `k.shop` instructions.
- Cleaner `k.r` (k.retainer) menu.
- Updated Color for B Class Retainers to be of a more distinct light blue/cyan color, the original was sort of a blue-white thing and was easily mistaken for a C class retainer
- `k.top` and `k.leaderboard` now shows 2 different leaderboards

## 9/20/2021

### Changed
- Fixed `k.exchange` Confirmation not showing up.
- Fixed “Retainer List Full” Error when you’re retainer list was not full


## 9/17/2021

### Added
- Added Wishlist Commands (`k.wish`, `k.wl`, `kwishremove`)

### Changed
- Fixed Error where new users weren’t able to register.
- Added retainer anime format to `k.getretainer`

## 9/16/2021

### Changed
- Removed `k.claim` Command completely since there’s buttons & reactions, the command has been obsolete for over a year now.
- Fixed user profiles not being correctly shown or not showing up at all.
- Fixed Common Retainers not spawning.
- Fixed Claims Invalid.
- Fixed Missing IDs for Ascended Retainers.
- Moved Retainer Models to a custom database rather than a JSON I’ve used for 2 years for Akane now.
- Checks will be executed every 6 hours rather than 3 hours to save resources.
